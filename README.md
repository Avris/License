# Avris License

Instead of attaching a license file to all of your projects,
you can generate a website with a license and have all the projects link to it.
That will let you keep it up-to-date.  

## Installation

    git clone git@gitlab.com:Avris/License.git
    cd License
    composer install
    cp license.yaml.dist license.yaml
    
## Customization

You can modify the `license.yaml` file to your needs, in particular:

 - add the copyright information (name, email, website)
 - name, short name and content of the license
 - theme (from `templates/theme/*`) and color (used by some themes and to generate a favicon)
 - turn on/off the avatar
 - footer
 
Since the year of first publication will differ for different projects, it is configurable in a GET parameter:

 - `/?year=2014` will render `Copyright © 2014`,
 - `/?year=2014i` will render `Copyright © 2014 – 2018`,
 - by default a year is not displayed (it's not necessary anyway).
 
The license is also available in plain text at `/license.txt`.
    
## Copyright

* **Author:** Andrzej Prusinowski [(Avris.it)](https://avris.it)
* **Huge inspiration**: [@remy](https://github.com/remy/mit-license)
* **Default license text**: [@jamiebuilds](https://github.com/jamiebuilds/license)
* **Licence:** [MIT](https://mit.avris.it)
