<?php

namespace App\Controller;

use App\Entity\License;
use App\Service\FaviconService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class LicenseController extends AbstractController
{
    /**
     * @Route("/")
     * @Route("/license.{_format}", requirements={"_format": "html|txt"})
     */
    public function show(Request $request)
    {
        return $this->render(
            sprintf('license/show.%s.twig', $request->getRequestFormat()),
            [
                'year' => $request->query->get('year'),
            ]
        );
    }

    /**
     * @Route("/favicon.png")
     */
    public function favicon(string $projectDir, FaviconService $faviconService)
    {
        $license = License::fromFile($projectDir . '/license.yaml');

        return new Response(
            $faviconService->generate($license),
            200,
            ['content-type' => 'image/png']
        );
    }
}
