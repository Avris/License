<?php

namespace App\Entity;

use Symfony\Component\Yaml\Yaml;

final class License
{
    /** @var string */
    private $name;

    /** @var string|null */
    private $shortName;

    /** @var string */
    private $ownerName;

    /** @var string */
    private $ownerEmail;

    /** @var string|null */
    private $ownerWebsite;

    /** @var string */
    private $content;

    /** @var string */
    private $theme;

    /** @var string */
    private $color;

    /** @var bool */
    private $avatar;

    /** @var string|null */
    private $footer;

    public static function fromFile(string $path): self
    {
        if (!file_exists($path)) {
            throw new \Exception(sprintf('File "%s" not found. You can create it based on license.yaml.dist', $path));
        }

        $data = Yaml::parse(file_get_contents($path));

        $license = new self;
        $license->name = $data['name'];
        $license->shortName = $data['shortName'] ?? null;
        $license->ownerName = $data['owner']['name'];
        $license->ownerEmail = $data['owner']['email'];
        $license->ownerWebsite = $data['owner']['website'] ?? null;
        $license->content = $data['content'];
        $license->theme = $data['theme'] ?? 'material';
        $license->color = $data['color'] ?? '#4a8a67';
        $license->avatar = (bool) $data['avatar'] ?? false;
        $license->footer = $data['footer'] ?? false;

        return $license;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getShortName(): ?string
    {
        return $this->shortName;
    }

    public function getOwnerName(): string
    {
        return $this->ownerName;
    }

    public function getOwnerEmail(): string
    {
        return $this->ownerEmail;
    }

    public function getOwnerWebsite(): ?string
    {
        return $this->ownerWebsite;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getTheme(): string
    {
        return $this->theme;
    }

    public function getColor(): string
    {
        return $this->color;
    }

    public function isAvatar(): bool
    {
        return $this->avatar;
    }

    public function getFooter(): ?string
    {
        return $this->footer;
    }
}
