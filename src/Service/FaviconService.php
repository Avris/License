<?php

namespace App\Service;

use App\Entity\License;
use Imagine\Gd\Font;
use Imagine\Image\Box;
use Imagine\Image\ImagineInterface;
use Imagine\Image\Palette\RGB;
use Imagine\Image\Point;

final class FaviconService
{
    /** @var ImagineInterface */
    private $imagine;

    /** @var string */
    private $fontFile;

    public function __construct(ImagineInterface $imagine, string $fontFile)
    {
        $this->imagine = $imagine;
        $this->fontFile = $fontFile;
    }

    public function generate(License $license): string
    {
        $name = str_replace(' ', "\n", $license->getShortName() ?: $license->getName());

        $palette = new RGB();

        $font = new Font($this->fontFile, 24, $palette->color('fff'));

        $textBox = $font->box($name);
        $imgSize = max($textBox->getWidth(), $textBox->getHeight()) * 1.2;

        $image = $this->imagine->create(
            new Box($imgSize, $imgSize),
            $palette->color($license->getColor())
        );

        $image->draw()->text(
            $name,
            $font,
            new Point(
                $imgSize / 2 - $textBox->getWidth() / 2,
                $imgSize / 2 - $textBox->getHeight() / 2
            )
        );

        $image->resize(new Box(32, 32));

        return $image->get('png');
    }
}
