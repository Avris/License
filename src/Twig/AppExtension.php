<?php

namespace App\Twig;

use App\Entity\License;
use Twig\Extension\AbstractExtension;
use Twig\Extension\GlobalsInterface;
use Twig\TwigFilter;

final class AppExtension extends AbstractExtension implements GlobalsInterface
{
    /** @var \Parsedown */
    private $markdown;

    /** @var string */
    private $projectDir;

    public function __construct(\Parsedown $markdown, string $projectDir)
    {
        $this->markdown = $markdown;
        $this->projectDir = $projectDir;
    }

    public function getGlobals(): array
    {
        return [
            'license' => License::fromFile($this->projectDir . '/license.yaml'),
        ];
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter(
                'markdown',
                function (string $text): string {
                    return $this->markdown->parse($text);
                },
                ['is_safe' => ['html']]
            ),
            new TwigFilter(
                'cleanUrl',
                function (string $url): string {
                    $url = trim($url);
                    $url = str_replace(['http://', 'https://', 'http://www.', 'https://www.'], '', $url);
                    if (strpos($url, '?')) {
                        $url = substr($url, 0, strpos($url, '?'));
                    }

                    return trim($url, '/');
                }
            ),
            new TwigFilter(
                'copyrightYear',
                function(?string $year): string {
                    if (!$year || !preg_match('#^(19|20\d\d)(i?)$#', $year, $matches)) {
                        return '';
                    }

                    return $matches[2] === 'i'
                        ? sprintf('%s – %s', $matches[1], date('Y'))
                        : $matches[1];
                }
            ),
            new TwigFilter(
                'gravatar',
                function (string $email, int $size = 36) :string {
                    return sprintf(
                        'https://www.gravatar.com/avatar/%s?s=%s',
                        md5(strtolower(trim($email))),
                        $size
                    );
                }
            ),
        ];
    }
}
